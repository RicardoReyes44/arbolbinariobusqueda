'''
Created on 13 nov. 2020

@author: RSSpe
'''

from nodo_arbol import NodoArbol


class ArbolBinarioBusqueda:

    def __init__(self):

        self.__raiz = None
    
    
    @property
    def raiz(self):

        return self.__raiz


    def insertar(self, dato):

        self.__raiz = self.__insertar2(self.raiz, dato)


    def __insertar2(self, padre, dato):

        if padre==None:
            padre = NodoArbol(dato)
        elif padre.mayorQue(dato):
            padre.izquierda = self.__insertar2(padre.izquierda, dato)
        else:
            padre.derecha = self.__insertar2(padre.derecha, dato)

        return padre


    def inorden(self, raiz):

        if raiz!=None:
            self.inorden(raiz.izquierda)
            print(raiz)
            self.inorden(raiz.derecha)


    def postorden(self, raiz):

        if raiz!=None:
            self.postorden(raiz.izquierda)
            self.postorden(raiz.derecha)
            print(raiz)


    def preorden(self, raiz):

        if raiz!=None:
            print(raiz)
            self.preorden(raiz.izquierda)
            self.preorden(raiz.derecha)


    def eliminar(self, dato):

        self.__raiz = self.__eliminar2(self.__raiz, dato)


    def __eliminar2(self, padre, dato):

        if padre==None:
            print("No existe el elemento")
        elif padre.mayorQue(dato):
            padre.izquierda = self.__eliminar2(padre.izquierda, dato)
        elif padre.menorQue(dato):
            padre.derecha = self.__eliminar2(padre.derecha, dato)
        else:

            if padre.izquierda==None:
                padre = padre.derecha
            elif padre.derecha==None:
                padre = padre.izquierda
            else:
                self.__eliminarDobleNodo(padre, dato)
            print("Elemento eliminado")

        return padre


    def __eliminarDobleNodo(self, padre, dato):

        anterior = padre
        actual = padre.izquierda

        while(actual.derecha!=None):
            anterior = actual
            actual = actual.derecha

        padre.dato = actual.dato

        if padre==anterior:
            anterior.izquierda = actual.izquierda
        else:
            anterior.derecha = actual.izquierda


    def buscar(self, dato):
        
        self.__buscar2(self.__raiz, dato)


    def __buscar2(self, padre, dato):

        if padre==None:
            print("No existe el elemento")
        elif padre.mayorQue(dato):
            return self.__buscar2(padre.izquierda, dato)
        elif padre.menorQue(dato):
            return self.__buscar2(padre.derecha, dato)
        else:
            print("Elemento encontrado")


    def contar(self):

        if self.__raiz==None:
            return 0
        else:
            return self.__contarHojas(self.__raiz)


    def __contarHojas(self, padre):

        if padre.izquierda==None and padre.derecha==None:
            return 1
        elif padre.izquierda==None:
            return self.__contarHojas(padre.derecha)
        elif padre.derecha==None:
            return self.__contarHojas(padre.izquierda)
        else:
            return self.__contarHojas(padre.izquierda) + self.__contarHojas(padre.derecha)


    def mayor(self):
        if self.__raiz==None:
            print("No hay elementos")

        else:
            self.__obtenerMayor(self.__raiz)


    def __obtenerMayor(self, padre):
        if padre.derecha==None:
            print("Mayor:",padre)
        else:
            self.__obtenerMayor(padre.derecha)


    def menor(self):
        if self.__raiz==None:
            print("No hay elementos")

        else:
            self.__obtenerMenor(self.__raiz)


    def __obtenerMenor(self, padre):
        if padre.izquierda==None:
            print("Menor:",padre)
        else:
            self.__obtenerMenor(padre.izquierda)
