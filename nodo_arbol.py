'''
Created on 13 nov. 2020

@author: RSSpe
'''

class NodoArbol:

    def __init__(self, dato):
        self.__dato = dato
        self.__izquierda = None
        self.__derecha = None


    @property
    def dato(self):
        return self.__dato


    @dato.setter
    def dato(self, dato):
        self.__dato = dato


    @property
    def izquierda(self):
        return self.__izquierda


    @izquierda.setter
    def izquierda(self, izquierda):
        self.__izquierda = izquierda


    @property
    def derecha(self):
        return self.__derecha


    @derecha.setter
    def derecha(self, derecha):
        self.__derecha = derecha


    def __str__(self):
        return f"Nodo=[dato: {self.__dato}]"


    def mayorQue(self, dato):
        return self.__dato>dato


    def menorQue(self, dato):
        return self.__dato<dato


    def mayorIgualQue(self, dato):
        return self.__dato>=dato


    def menorIgualQue(self, dato):
        return self.__dato<=dato


    def igualQue(self, dato):
        return self.__dato==dato
