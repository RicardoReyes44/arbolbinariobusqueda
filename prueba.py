'''
Created on 13 nov. 2020

@author: RSSpe
'''

from arbol_binario_busqueda import ArbolBinarioBusqueda


abb = ArbolBinarioBusqueda()

def elegir():
    
    while(True):
        try:
            print("\n------------------SUBMENU-------------------")
            print("1.- Inorden")
            print("2.- Preorden")
            print("3.- Postorden")
            subOpcion = int(input("Introduce opcion de recorrido: "))

            if subOpcion==1 or subOpcion==2 or subOpcion==3:
                return subOpcion

            else:
                print("Opcion inexistente, por favor vuelve a intentarlo")

        except ValueError as error:
            print("Error <", error, "> por favor vuelve a intentarlo")
        print()

while(True):
    
    try:
        print("------------------MENU-------------------")
        print("1.- Crear nuevo arbol")
        print("2.- Insertar nodo")
        print("3.- Eliminar nodo")
        print("4.- Mostrar nodos")
        print("5.- Mostrar dato mayor")
        print("6.- Mostrar dato menor")
        print("7.- Buscar dato")
        print("8.- Mostrar cantidad de nodos hoja")
        print("9.- Salir")
        opcion = int(input("Introduce opcion: "))

        if opcion==1:
            abb = ArbolBinarioBusqueda()
            print("Se ha creado un nuevo arbol")

        elif opcion==2:
            dato = int(input("Introduce un numero: "))
            abb.insertar(dato)

        elif opcion==3:
            dato = int(input("Introduce un numero: "))
            abb.eliminar(dato)

        elif opcion==4:
            if abb.raiz==None:
                print("No hay elementos")

            else:
                subOpcion = elegir()
                
                if subOpcion==1:
                    abb.inorden(abb.raiz)

                elif subOpcion==2:
                    abb.preorden(abb.raiz)

                else:
                    abb.postorden(abb.raiz)

        elif opcion==5:
            abb.menor()

        elif opcion==6:
            abb.mayor()

        elif opcion==7:
            dato = int(input("Introduce un numero: "))
            abb.buscar(dato)

        elif opcion==8:
            print("Cantidad de nodos hoja:",abb.contar())

        elif opcion==9:
            print("\n\n----------------------\nFin del programa\n----------------------\n")
            break

        else:
            print("Opcion inexistente, por favor prueba de nuevo")

    except ValueError as error:
        print("Error <", error, "> por favor vuelve a intentarlo")
        
    print()
    print("--------------------------------------")
